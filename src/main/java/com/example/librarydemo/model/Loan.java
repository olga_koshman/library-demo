package com.example.librarydemo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Loan {

    private Book book;
    private Reader reader;

}
