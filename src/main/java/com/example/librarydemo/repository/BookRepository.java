package com.example.librarydemo.repository;

import com.example.librarydemo.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> getAllByIsAvailableTrue();

    List<Book> getAllByReaderId(Long readerId);
}
