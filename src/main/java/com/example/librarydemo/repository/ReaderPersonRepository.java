package com.example.librarydemo.repository;

import com.example.librarydemo.model.Reader;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReaderPersonRepository extends JpaRepository<Reader, Long> {
}
