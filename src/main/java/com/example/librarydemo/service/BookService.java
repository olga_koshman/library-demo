package com.example.librarydemo.service;

import com.example.librarydemo.model.Author;
import com.example.librarydemo.model.Book;
import com.example.librarydemo.model.Loan;
import com.example.librarydemo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    private BookRepository bookRepository;
    private AuthorService authorService;

    @Autowired
    public BookService(BookRepository bookRepository, AuthorService authorService) {
        this.bookRepository = bookRepository;
        this.authorService = authorService;
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public List<Book> getAllAvailableBooks() {
        return bookRepository.getAllByIsAvailableTrue();
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public Book getOneById(Long id) {
        return bookRepository.findById(id).get();
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public void setBookAvailability(Loan loan) {
        loan.getBook().setIsAvailable(!loan.getBook().getIsAvailable());
        loan.getBook().setReader(loan.getReader());
        bookRepository.save(loan.getBook());
    }

    public void releaseBooksFromReader(Long readerId) {
        List<Book> booksFromReader = bookRepository.getAllByReaderId(readerId);
        for (Book book : booksFromReader) {
            book.setIsAvailable(true);
            book.setReader(null);
            bookRepository.save(book);
        }
    }
}
