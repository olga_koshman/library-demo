package com.example.librarydemo.service;

import com.example.librarydemo.model.Loan;
import com.example.librarydemo.model.Reader;
import com.example.librarydemo.repository.ReaderPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReaderPersonService {

    private ReaderPersonRepository readerPersonRepository;

    @Autowired
    public ReaderPersonService(ReaderPersonRepository readerPersonRepository) {
        this.readerPersonRepository = readerPersonRepository;
    }

    public List<Reader> getAllReaderPersons() {
        return readerPersonRepository.findAll();
    }

    public Reader getReaderPersonById(Long id) {
        return readerPersonRepository.findById(id).get();
    }

    public Reader saveReaderPerson(Reader reader) {
        return readerPersonRepository.save(reader);
    }

    public void deleteReaderPerson(Long id) {
        readerPersonRepository.deleteById(id);
    }

    public void assignBookToReader(Loan loan) {
        loan.getReader().getBooks().add(loan.getBook());
        readerPersonRepository.save(loan.getReader());
    }
}
