package com.example.librarydemo.service;

import com.example.librarydemo.model.Genre;
import com.example.librarydemo.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService {

    private GenreRepository genreRepository;

    @Autowired
    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    public Genre createGenre(Genre genre) {
        return genreRepository.save(genre);
    }

    public Genre getOneById(Long id) {
        return genreRepository.findById(id).get();
    }

    public void deleteGenre(Long id) {
        genreRepository.deleteById(id);
    }
}
