package com.example.librarydemo.service;

import com.example.librarydemo.model.Library;
import com.example.librarydemo.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibraryService {

    private LibraryRepository libraryRepository;

    @Autowired
    public LibraryService(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    public List<Library> getAllLibraries() {
        return libraryRepository.findAll();
    }

    public Library createLibrary(Library library) {
        return libraryRepository.save(library);
    }

    public Library getOneById(Long id) {
        return libraryRepository.findById(id).get();
    }

    public void deleteLibrary(Long id) {
        libraryRepository.deleteById(id);
    }

}
