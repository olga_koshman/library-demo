package com.example.librarydemo.service;

import com.example.librarydemo.model.Author;
import com.example.librarydemo.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Author getOneById(long id) {
        return authorRepository.findById(id).get();
    }

    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    public Author createOne(Author author) {
        return authorRepository.save(author);
    }

    public void deleteAuthor(Long id) {
        authorRepository.deleteById(id);
    }

}
