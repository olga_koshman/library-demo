package com.example.librarydemo.controller;

import com.example.librarydemo.model.*;
import com.example.librarydemo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/main")
public class MainController {

    private AuthorService authorService;
    private BookService bookService;
    private GenreService genreService;
    private LibraryService libraryService;
    private ReaderPersonService readerPersonService;

    @Autowired
    public MainController(AuthorService authorService, BookService bookService,
                          GenreService genreService, LibraryService libraryService,
                          ReaderPersonService readerPersonService) {
        this.authorService = authorService;
        this.bookService = bookService;
        this.genreService = genreService;
        this.libraryService = libraryService;
        this.readerPersonService = readerPersonService;
    }

    @GetMapping("/edit/add-author")
    public String showCreateAuthorForm(Author author) {
        return "/edit/add-author";
    }

    @GetMapping("/edit/add-book")
    public String showCreateBookForm(Book book, Model model) {
        model.addAttribute("authors", authorService.getAllAuthors());
        model.addAttribute("genres", genreService.getAllGenres());
        model.addAttribute("libraries", libraryService.getAllLibraries());
        return "/edit/add-book";
    }

    @GetMapping("/edit/add-genre")
    public String showCreateGenreForm(Genre genre) {
        return "/edit/add-genre";
    }

    @GetMapping("/edit/add-library")
    public String showCreateLibraryForm(Library library) {
        return "/edit/add-library";
    }

    @GetMapping("/edit/add-reader")
    public String showCreateReaderForm(Reader person) {
        return "/edit/add-reader";
    }

    @GetMapping("/edit/author/{id}")
    public String showEditAuthorForm(@PathVariable Long id, Model model) {
        model.addAttribute("author", authorService.getOneById(id));
        return "/edit/add-author";
    }

    @GetMapping("/edit/book/{id}")
    public String showEditBookForm(@PathVariable Long id, Model model) {
        model.addAttribute("book", bookService.getOneById(id));
        model.addAttribute("authors", authorService.getAllAuthors());
        model.addAttribute("genres", genreService.getAllGenres());
        model.addAttribute("libraries", libraryService.getAllLibraries());
        return "/edit/add-book";
    }

    @GetMapping("/edit/genre/{id}")
    public String showEditGenreForm(@PathVariable Long id, Model model) {
        model.addAttribute("genre", genreService.getOneById(id));
        return "/edit/add-genre";
    }

    @GetMapping("/edit/library/{id}")
    public String showEditLibraryForm(@PathVariable Long id, Model model) {
        model.addAttribute("library", libraryService.getOneById(id));
        return "/edit/add-library";
    }

    @GetMapping("/edit/reader/{id}")
    public String showEditReaderForm(@PathVariable Long id, Model model) {
        model.addAttribute("reader", readerPersonService.getReaderPersonById(id));
        return "/edit/add-reader";
    }

    @GetMapping("/delete/author/{id}")
    public String deleteAuthor(@PathVariable Long id, Model model) {
        authorService.deleteAuthor(id);
        return "redirect:/main/authors";
    }

    @GetMapping("/delete/book/{id}")
    public String deleteBook(@PathVariable Long id, Model model) {
        bookService.deleteBook(id);
        return "redirect:/main/books";
    }

    @GetMapping("/delete/genre/{id}")
    public String deleteGenre(@PathVariable Long id, Model model) {
        genreService.deleteGenre(id);
        return "redirect:/main/genres";
    }

    @GetMapping("/delete/library/{id}")
    public String deleteLibrary(@PathVariable Long id, Model model) {
        libraryService.deleteLibrary(id);
        return "redirect:/main/libraries";
    }

    @GetMapping("/delete/reader/{id}")
    public String deleteReader(@PathVariable Long id, Model model) {
        bookService.releaseBooksFromReader(id);
        readerPersonService.deleteReaderPerson(id);
        return "redirect:/main/readers";
    }

    @GetMapping("/authors")
    public String getAuthors(Model model) {
        model.addAttribute("authors", authorService.getAllAuthors());
        return "authors";
    }

    @GetMapping("/books")
    public String getBooks(Model model) {
        model.addAttribute("books", bookService.getAllBooks());
        return "books";
    }

    @GetMapping("/genres")
    public String getGenres(Model model) {
        model.addAttribute("genres", genreService.getAllGenres());
        return "genres";
    }

    @GetMapping("/libraries")
    public String getLibraries(Model model) {
        model.addAttribute("libraries", libraryService.getAllLibraries());
        return "libraries";
    }

    @GetMapping("/readers")
    public String getReaders(Model model) {
        model.addAttribute("readers", readerPersonService.getAllReaderPersons());
        return "readers";
    }

    @PostMapping("/edit/createauthor")
    public String createAuthor(Author author, BindingResult result, Model model) {
        authorService.createOne(author);
        return "redirect:/main/authors";
    }

    @PostMapping("/edit/createbook")
    public String createBook(Book book) {
        bookService.createBook(book);
        return "redirect:/main/books";
    }

    @PostMapping("/edit/creategenre")
    public String createGenre(Genre genre) {
        genreService.createGenre(genre);
        return "redirect:/main/genres";
    }

    @PostMapping("/edit/createlibrary")
    public String createLibrary(Library library) {
        libraryService.createLibrary(library);
        return "redirect:/main/libraries";
    }

    @PostMapping("/edit/createreader")
    public String createReader(Reader person) {
        readerPersonService.saveReaderPerson(person);
        return "redirect:/main/readers";
    }

    @GetMapping("/loan")
    public String showLoanForm(Loan loan, Model model) {
        model.addAttribute("books", bookService.getAllAvailableBooks());
        model.addAttribute("readers", readerPersonService.getAllReaderPersons());
        return "/add-loan";
    }

    @PostMapping("/createloan")
    public String createLoan(Loan loan, BindingResult result, Model model) {
        readerPersonService.assignBookToReader(loan);
        bookService.setBookAvailability(loan);
        return "redirect:/";
    }
}
