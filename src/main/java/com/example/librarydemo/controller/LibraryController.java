package com.example.librarydemo.controller;

import com.example.librarydemo.model.Library;
import com.example.librarydemo.service.LibraryService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/library")
public class LibraryController {

    private LibraryService libraryService;

    @Autowired
    public LibraryController(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @GetMapping
    public List<Library> getAll() {
        return libraryService.getAllLibraries();
    }

    @GetMapping("/test")
    public List<Library> getOne() {
        return libraryService.getAllLibraries();
    }

    @PostMapping("/create")
    public Library createLibrary(@RequestBody Library library) {
        return libraryService.createLibrary(library);
    }

}
