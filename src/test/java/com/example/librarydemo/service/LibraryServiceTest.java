package com.example.librarydemo.service;

import com.example.librarydemo.model.Book;
import com.example.librarydemo.model.Library;
import com.example.librarydemo.repository.BookRepository;
import com.example.librarydemo.repository.LibraryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.MockitoAnnotations.initMocks;

public class LibraryServiceTest {
    @InjectMocks
    private LibraryService libraryService;

    @Mock
    private LibraryRepository libraryRepository;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    void testGetAllLibraries() {
        Library library1 = new Library();
        Library library2 = new Library();
        library1.setAddress("Leninsky");
        library2.setAddress("Veteranov");
        List<Library> libraries = new ArrayList<>();
        libraries.add(library1);
        libraries.add(library2);
        Mockito.when(libraryRepository.findAll()).thenReturn(libraries);

        List<Library> libraryList = libraryService.getAllLibraries();

        Mockito.verify(libraryRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(libraries.size(), libraryList.size());
    }

    @Test
    void testGetOneLibraryById() {
        Library library = new Library();
        library.setAddress("Leninsky");
        Mockito.when(libraryRepository.findById(1L)).thenReturn(Optional.of(library));

        Library actual = libraryService.getOneById(1L);

        Mockito.verify(libraryRepository, Mockito.times(1)).findById(1L);
        Assertions.assertEquals(library.getAddress(), actual.getAddress());
    }

    @Test
    void testCreateOne() {
        Library library = new Library();
        library.setAddress("Leninsky");
        Mockito.when(libraryRepository.save(library)).thenReturn(library);

        Library actual = libraryService.createLibrary(library);

        Mockito.verify(libraryRepository, Mockito.times(1)).save(library);
        Assertions.assertEquals(library.getAddress(), actual.getAddress());
    }
}
