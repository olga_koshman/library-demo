package com.example.librarydemo.service;

import com.example.librarydemo.model.Author;
import com.example.librarydemo.repository.AuthorRepository;
import org.aspectj.lang.annotation.Before;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.MockitoAnnotations.initMocks;

public class AuthorServiceTest {

    @InjectMocks
    private AuthorService authorService;

    @Mock
    private AuthorRepository authorRepository;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    void testGetOneAuthorById() {
        Author author = new Author();
        author.setFirstName("Apple");
        Mockito.when(authorRepository.findById(1L)).thenReturn(Optional.of(author));

        Author actual = authorService.getOneById(1L);

        Mockito.verify(authorRepository, Mockito.times(1)).findById(1L);
        Assertions.assertEquals(author.getFirstName(), actual.getFirstName());
    }
    @Test
    void testCreateOne() {
        Author author = new Author();
        author.setFirstName("Apple");
        Mockito.when(authorRepository.save(author)).thenReturn(author);

        Author actual = authorService.createOne(author);

        Mockito.verify(authorRepository, Mockito.times(1)).save(author);
        Assertions.assertEquals(author.getFirstName(), actual.getFirstName());
    }
    @Test
    void testGetAllAuthors() {
        Author author1 = new Author();
        Author author2 = new Author();
        author1.setFirstName("Apple");
        author2.setFirstName("Bars");
        List<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);
        Mockito.when(authorRepository.findAll()).thenReturn(authors);

        List<Author>  authorList = authorService.getAllAuthors();

        Mockito.verify(authorRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(authors.size(), authorList.size());
    }
}
