package com.example.librarydemo.service;

import com.example.librarydemo.model.Book;
import com.example.librarydemo.model.Reader;
import com.example.librarydemo.repository.BookRepository;
import com.example.librarydemo.repository.ReaderPersonRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.MockitoAnnotations.initMocks;

public class ReaderPersonServiceTest {
    @InjectMocks
    private ReaderPersonService readerPersonService;

    @Mock
    private ReaderPersonRepository readerPersonRepository;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    void testGetAllReaderPersons() {
        Reader reader1 = new Reader();
        Reader reader2 = new Reader();
        reader1.setEmail("qwe@mail.ru");
        reader2.setEmail("asd@mail.ru");
        List<Reader> readers = new ArrayList<>();
        readers.add(reader1);
        readers.add(reader2);
        Mockito.when(readerPersonRepository.findAll()).thenReturn(readers);

        List<Reader> readerList = readerPersonService.getAllReaderPersons();

        Mockito.verify(readerPersonRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(readers.size(), readerList.size());
    }

    @Test
    void testGetOneReaderById() {
        Reader reader = new Reader();
        reader.setEmail("qwe@mail.ru");
        Mockito.when(readerPersonRepository.findById(1L)).thenReturn(Optional.of(reader));

        Reader actual = readerPersonService.getReaderPersonById(1L);

        Mockito.verify(readerPersonRepository, Mockito.times(1)).findById(1L);
        Assertions.assertEquals(reader.getEmail(), actual.getEmail());
    }

    @Test
    void testCreateOne() {
        Reader reader = new Reader();
        reader.setEmail("qwe@mail.ru");
        Mockito.when(readerPersonRepository.save(reader)).thenReturn(reader);

        Reader actual = readerPersonService.saveReaderPerson(reader);

        Mockito.verify(readerPersonRepository, Mockito.times(1)).save(reader);
        Assertions.assertEquals(reader.getEmail(), actual.getEmail());
    }
}
