package com.example.librarydemo.service;

import com.example.librarydemo.model.Author;
import com.example.librarydemo.model.Book;
import com.example.librarydemo.repository.AuthorRepository;
import com.example.librarydemo.repository.BookRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.MockitoAnnotations.initMocks;

public class BookServiceTest {

    @InjectMocks
    private BookService bookService;

    @Mock
    private BookRepository bookRepository;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    void testGetAllBooks() {
        Book book1 = new Book();
        Book book2 = new Book();
        book1.setTitle("lykomorie");
        book2.setTitle("Kolobok");
        List<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        Mockito.when(bookRepository.findAll()).thenReturn(books);

        List<Book> bookList = bookService.getAllBooks();

        Mockito.verify(bookRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(books.size(), bookList.size());
    }

    @Test
    void testGetOneBookById() {
        Book book = new Book();
        book.setTitle("Kukomorye");
        Mockito.when(bookRepository.findById(1L)).thenReturn(Optional.of(book));

        Book actual = bookService.getOneById(1L);

        Mockito.verify(bookRepository, Mockito.times(1)).findById(1L);
        Assertions.assertEquals(book.getTitle(), actual.getTitle());
    }

    @Test
    void testCreateOne() {
        Book book = new Book();
        book.setTitle("Kolobok");
        Mockito.when(bookRepository.save(book)).thenReturn(book);

        Book actual = bookService.createBook(book);

        Mockito.verify(bookRepository, Mockito.times(1)).save(book);
        Assertions.assertEquals(book.getTitle(), actual.getTitle());
    }
}
