package com.example.librarydemo.service;

import com.example.librarydemo.model.Book;
import com.example.librarydemo.model.Genre;
import com.example.librarydemo.repository.BookRepository;
import com.example.librarydemo.repository.GenreRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.MockitoAnnotations.initMocks;

public class GenreServiceTest {
    @InjectMocks
    private GenreService genreService;

    @Mock
    private GenreRepository genreRepository;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }

    @Test
    void testGetOneGenreById() {
        Genre genre = new Genre();
        genre.setName("Fairytale");
        Mockito.when(genreRepository.findById(1L)).thenReturn(Optional.of(genre));

        Genre actual = genreService.getOneById(1L);

        Mockito.verify(genreRepository, Mockito.times(1)).findById(1L);
        Assertions.assertEquals(genre.getName(), actual.getName());
    }

    @Test
    void testGetAllGenre() {
        Genre genre1 = new Genre();
        Genre genre2 = new Genre();
        genre1.setName("Fairytale");
        genre2.setName("Horror");
        List<Genre> genres = new ArrayList<>();
        genres.add(genre1);
        genres.add(genre2);
        Mockito.when(genreRepository.findAll()).thenReturn(genres);

        List<Genre> genreList = genreService.getAllGenres();

        Mockito.verify(genreRepository, Mockito.times(1)).findAll();
        Assertions.assertEquals(genres.size(), genreList.size());
    }

    @Test
    void testCreateGenre() {
        Genre genre = new Genre();
        genre.setName("Horror");
        Mockito.when(genreRepository.save(genre)).thenReturn(genre);

        Genre actual = genreService.createGenre(genre);

        Mockito.verify(genreRepository, Mockito.times(1)).save(genre);
        Assertions.assertEquals(genre.getName(), actual.getName());
    }
}
