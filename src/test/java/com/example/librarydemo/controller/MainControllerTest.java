package com.example.librarydemo.controller;

import com.example.librarydemo.model.*;
import com.example.librarydemo.service.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private AuthorService authorService;
    @Mock
    private BookService bookService;
    @Mock
    private LibraryService libraryService;
    @Mock
    private GenreService genreService;
    @Mock
    private ReaderPersonService readerPersonService;

    @Test
    void findOneAuthorByIdAndAddToModel() throws Exception {
        Author author = new Author();
        author.setFirstName("Apple");
        when(authorService.getOneById(1L)).thenReturn(author);

        mockMvc.perform(get("/main/edit/author/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/edit/add-author"))
                .andExpect(model().attributeExists("author"));

        verifyNoMoreInteractions(authorService);
    }

    @Test
    void findOneBookByIdAndAddToModel() throws Exception {
        Book book = new Book();
        book.setTitle("Lukomorye");
        when(bookService.getOneById(1L)).thenReturn(book);

        mockMvc.perform(get("/main/edit/book/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/edit/add-book"))
                .andExpect(model().attributeExists("book"));

        verifyNoMoreInteractions(bookService);
    }

    @Test
    void findOneLibraryByIdAndAddToModel() throws Exception {
        Library library = new Library();
        library.setName("Kolobok");
        when(libraryService.getOneById(1L)).thenReturn(library);

        mockMvc.perform(get("/main/edit/library/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/edit/add-library"))
                .andExpect(model().attributeExists("library"));

        verifyNoMoreInteractions(libraryService);
    }
    @Test
    void findOneGenreByIdAndAddToModel() throws Exception {
       Genre genre = new Genre();
        genre.setName("Fairytale");
        when(genreService.getOneById(1L)).thenReturn(genre);

        mockMvc.perform(get("/main/edit/genre/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/edit/add-genre"))
                .andExpect(model().attributeExists("genre"));

        verifyNoMoreInteractions(genreService);
    }

    @Test
    void findOneReaderByIdAndAddToModel() throws Exception {
        Reader reader = new Reader();
        reader.setLastName("Ivanov");
        when(readerPersonService.getReaderPersonById(1L)).thenReturn(reader);

        mockMvc.perform(get("/main/edit/reader/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/edit/add-reader"))
                .andExpect(model().attributeExists("reader"));

        verifyNoMoreInteractions(readerPersonService);
    }
}
